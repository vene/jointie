import os
import re
import json
import random
from itertools import chain
from collections import Counter, OrderedDict

import numpy as np

import pickle
import joblib

WEIRD_DATE = re.compile(r'\d\d\d\d-\d\d-\d\dt\d\d')
HAS_DIGIT = re.compile(r'.*\d')
HAS_LETTER = re.compile(r'.*[a-z]')


def entity(label):
    if label == 'O':
        return 'O'
    else:
        position, entity_type, *_ = label.split("_")
        return f"{position}_{entity_type}"


def process(word):
    word = word.lower()
    if "_" in word:
        return '__DOC__'
    elif WEIRD_DATE.match(word):
        return '__NUM__'
    elif HAS_DIGIT.match(word):
        if HAS_LETTER.match(word):
            return word
        else:
            return '__NUM__'
    return word


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--data')
    parser.add_argument('--glove')
    parser.add_argument('--out', default='data/ace')
    opts = parser.parse_args()

    def _iter_glove():
        with open(opts.glove) as f:
            for line in f:
                word, embed = line.split(' ', 1)
                yield word, embed

    bilou = os.path.join(opts.data, 'BILOU_annotations.txt')
    sents = os.path.join(opts.data, 'sentence_id.txt')

    train_files = os.path.join(opts.data, 'filelist_ACE05',
                               'filelist_train.lst')
    valid_files = os.path.join(opts.data, 'filelist_ACE05',
                               'filelist_dev.lst')
    test_files = os.path.join(opts.data, 'filelist_ACE05',
                               'filelist_test.lst')

    split = dict()

    def get_fnames(fn):
        with open(fn) as f:
            names = f.readlines()

        roots = set('/' + name.strip().rsplit('/', 1)[-1] for name in names)
        assert len(roots) == len(names)
        return roots

    split.update({name: 'train' for name in get_fnames(train_files)})
    split.update({name: 'valid' for name in get_fnames(valid_files)})
    split.update({name: 'test' for name in get_fnames(test_files)})

    with open(bilou) as f, open(sents) as f_sents:

        sent_doc = [line.strip().split()[-1] for line in f_sents]

        data = {'train': [], 'valid': [], 'test': []}
        this_words = []
        this_labels = []

        k = 0

        # chain an empty sentence at the end
        for line in chain(f, ''):

            line = line.strip()

            if not line:
                # decide if train / valid / test
                dset = split[sent_doc[k]]
                data[dset].append((this_words, this_labels))
                this_words = []
                this_labels = []
                k += 1

            else:
                word, label, tag = line.split('\t')
                this_words.append(process(word))
                this_labels.append(entity(label))

    word_dict = OrderedDict()
    word_dict['__PAD__'] = 0
    word_dict['__UNK__'] = 1
    word_dict['__DOC__'] = 2
    word_dict['__NUM__'] = 3

    train_words = Counter(w for seq, _ in data['train']
                          for w in seq
                          if not w.startswith("__"))
    hapax = {w: False for w, c in train_words.items() if c == 1}

    # check if words with count 1 exist in glove. If so, keep them.
    for word, embed in _iter_glove():
        if word in hapax:
            hapax[word] = True

    for word, flag in hapax.items():
        if not flag:
            del train_words[word]

    k = 1 + max(word_dict.values())
    for word, _ in train_words.most_common():
        word_dict[word] = k
        k += 1

    def vectorize(seq):
        return [word_dict.get(word, word_dict['__UNK__']) for word in seq]

    data_train = [(vectorize(ws), ls) for ws, ls in data['train']]
    data_valid = [(vectorize(ws), ls) for ws, ls in data['valid']]
    data_test = [(vectorize(ws), ls) for ws, ls in data['test']]

    # extract Glove for words in vocab
    indices = []
    embeds = []
    for word, embed in _iter_glove():
        ix = word_dict.get(word)
        if ix is not None:
            indices.append(ix)
            embed = np.fromstring(embed, dtype=np.float32, sep=' ')
            embeds.append(embed)
    indices = np.array(indices, dtype=np.intp)
    embeds = np.row_stack(embeds)
    print(f"Coverage rate {len(indices)/len(word_dict) * 100}")

    joblib.dump(word_dict, os.path.join(opts.out, 'vocab.jbl'),
                protocol=pickle.HIGHEST_PROTOCOL)

    joblib.dump(data_train, os.path.join(opts.out, 'train.jbl'),
                protocol=pickle.HIGHEST_PROTOCOL)

    joblib.dump(data_valid, os.path.join(opts.out, 'valid.jbl'),
                protocol=pickle.HIGHEST_PROTOCOL)

    joblib.dump(data_test, os.path.join(opts.out, 'test.jbl'),
                protocol=pickle.HIGHEST_PROTOCOL)

    joblib.dump((indices, embeds), os.path.join(opts.out, 'embed.jbl'),
                protocol=pickle.HIGHEST_PROTOCOL)

