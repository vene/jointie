import numpy as np


def _to_entities(ys, cls):
    entities = []
    start = None
    for k, y in enumerate(ys):

        if y == f'U_{cls}':
            entities.append((k, k))
            start = None
        elif y == f'I_{cls}':
            pass  # don't reset start
        elif y[0] in ('O', 'I', 'U'):
            # by elimination, if I or U it is another class, so reset start
            start = None
        elif y == f'B_{cls}':
            start = k
        elif y == f'L_{cls}':
            if start is not None:
                entities.append((start, k))
            start = None

    return entities


def _count_predictions(y_true, y_pred, cls):
    true_spans = set(_to_entities(y_true, cls))
    pred_spans = set(_to_entities(y_pred, cls))

    tp = len(true_spans.intersection(pred_spans))
    fp = len(pred_spans - true_spans)
    fn = len(true_spans - pred_spans)

    return tp, fp, fn


def _p_r_f(tp, fp, fn):
    p = tp / (tp + fp + 1e-100)
    r = tp / (tp + fn + 1e-100)
    f1 = (2 * p * r) / (p + r + 1e-100)
    return p, r, f1


def bio_f_score_micro(y_true, y_pred, classes):
    tp = fp = fn = 0
    for cls in classes:
        tp_, fp_, fn_ = _count_predictions(y_true, y_pred, cls)

        tp += tp_
        fp += fp_
        fn += fn_

    p, r, f = _p_r_f(tp, fp, fn)
    return p, r, f


def bio_f_score_macro(y_true, y_pred, classes):

    f1s = []
    for cls in classes:
        tp, fp, fn = _count_predictions(y_true, y_pred, cls)
        _, _, f = _p_r_f(tp, fp, fn)
        f1s.append(f)

    return f1s

