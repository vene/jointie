import torch
from torch import nn
from torch.autograd import Variable
import numpy as np

from .utils import log_sum_exp

PAD = 0


class LSTMTagger(nn.Module):
    def __init__(self, vocab_size, label_dict, embed_dim=300, hidden_dim=128,
                 dropout_p=0.5):
        self.vocab_size = vocab_size
        self.label_dict = label_dict
        self.embed_dim = embed_dim
        self.hidden_dim = hidden_dim
        self.dropout_p = dropout_p

        self.n_classes = len(label_dict)
        self.label_inverse = np.empty(self.n_classes, dtype='U5')
        for y, k in label_dict.items():
            self.label_inverse[k] = y

        self.classes = sorted(list(set(y[2:] for y in label_dict if y != 'O')))

        super().__init__()
        self.build()

    def build(self):
        self.embed = nn.Embedding(self.vocab_size, self.embed_dim,
                                  padding_idx=PAD, sparse=True)

        self.rnn = nn.LSTM(self.embed_dim,
                           self.hidden_dim // 2,
                           bidirectional=True,
                           dropout=self.dropout_p)

        self.out = nn.Linear(self.hidden_dim, self.n_classes)
        self.loss_func = nn.CrossEntropyLoss(size_average=False)

    def initialize_embeddings(self, pre_indices, pre_weights):
        self.embed.weight.data[pre_indices] = pre_weights

    def forward(self, words):
        words_emb = self.embed(words)
        hid, _ = self.rnn(words_emb.unsqueeze(1))
        out = self.out(hid).squeeze(1)
        return out

    def to_labels(self, indices):
        return self.label_inverse[indices.numpy()]

    def predict(self, words):
        scores = self(words)
        scores = scores.cpu()
        _, y_pred = torch.max(scores.data, dim=1)
        return y_pred

    def loss(self, words, y_true):
        scores = self(word_ix)
        obj = self.loss_func(scores, y_true)
        obj /= len(word_ix)
        return obj


class LSTMCRFTagger(LSTMTagger):

    def build(self):
        super().build()
        self.initial = nn.Parameter(torch.Tensor(self.n_classes))
        self.final= nn.Parameter(torch.Tensor(self.n_classes))
        self.transition = nn.Parameter(torch.Tensor(self.n_classes,
                                       self.n_classes))
        nn.init.uniform(self.transition, -0.01, 0.01)
        nn.init.uniform(self.initial, -0.01, 0.01)
        nn.init.uniform(self.final, -0.01, 0.01)

    def _partition_function(self, unary_potentials):
        """Forward algorithm to compute log Z"""

        # for shorter math
        T = self.transition
        U = unary_potentials

        seq_length, _ = unary_potentials.size()
        alpha = self.initial + unary_potentials[0]
        for i in range(1, seq_length):
            #  next_a = []
            #  for t in range(self.n_classes):
                #  next_a_t = alpha + T[:, t] + U[i, t]
                #  next_a.append(log_sum_exp(next_a_t))
            #  alpha = torch.cat(next_a)
            alpha = log_sum_exp(alpha + T.t() + U[i].view(-1, 1), dim=1)

        alpha += self.final
        return log_sum_exp(alpha)


    def _viterbi(self, unary_potentials):
        T = self.transition.cpu()
        initial = self.initial.cpu()
        final = self.final.cpu()
        U = unary_potentials

        seq_length, n_tags = U.size()
        scores = Variable(U.data.new(U.size()).zero_())
        backpt = Variable(U.data.new(U.size()).long().zero_())

        scores[0] = initial + U[0]
        for i in range(1, seq_length):
            vals, ixs = torch.max(scores[i - 1].view(-1, 1) + T + U[i], dim=0)
            scores[i] = vals
            backpt[i] = ixs

        best_val, best_ix = torch.max(scores[-1] + final, dim=0)
        best_ix = best_ix.data[0]
        tags = [best_ix]

        i = seq_length - 1
        while i > 0:
            best_ix = backpt[i, best_ix]
            best_ix = best_ix.data[0]
            tags.append(best_ix)
            i -= 1
        tags.reverse()
        return best_val, tags

    def _seq_score(self, unary_potentials, y_true):
        seq_length, _ = unary_potentials.size()

        y_true_t = Variable(y_true)

        seq_score = unary_potentials.gather(1, y_true_t.view(-1, 1)).sum()

        if len(y_true) > 1:
            seq_score += (self.transition
                .index_select(dim=0, index=y_true_t[:-1])
                .gather(dim=1, index=y_true_t[1:].view(-1, 1))
                .sum())

        seq_score += self.initial[y_true[0]]
        seq_score += self.final[y_true[-1]]

        return seq_score

    def loss(self, word_ix, y_true):
        unaries = self(word_ix)
        seq_score = self._seq_score(unaries, y_true.data)
        logZ = self._partition_function(unaries)
        # log-likelihood = seq_score - logZ. Loss is negative LL
        return logZ - seq_score

    def predict(self, words):
        unaries = self(words)
        unaries = unaries.cpu()
        _, y_pred = self._viterbi(unaries)
        return torch.LongTensor(y_pred)

