import os
from time import time

import torch
from torch import nn
from torch.autograd import Variable

from torch.optim import SGD
from torch.optim.lr_scheduler import ReduceLROnPlateau


from .evaluate import bio_f_score_micro
from .seq_models import LSTMTagger, LSTMCRFTagger


_models = {
    'softmax': LSTMTagger,
    'crf': LSTMCRFTagger
}


try:
    from tqdm import tqdm
except ImportError:
    print("tqdm not available")

    def tqdm(x):
        return x


def score(model, seqs, cuda=False):
    model.eval()
    y_preds = []
    y_trues = [y for _, _, ys in seqs for y in ys]
    for word_ix, _, _ in seqs:
        y_pred = model.predict(word_ix)
        y_preds.append(y_pred)

    y_preds = model.to_labels(torch.cat(y_preds))
    p, r, f = bio_f_score_micro(y_trues, y_preds, model.classes)
    return p, r, f


def train(model, seqs_train, seqs_valid, n_epochs, checkpoint_fn,
          load_from=None, cuda=False):

    start_epoch = 0

    if cuda:
        model = model.cuda()

    print(f"Training model: {model}")

    optim = SGD(model.parameters(), lr=0.1)
    # learning rate is halved when valid accuracy stops increasing
    sched = ReduceLROnPlateau(optim,
                              mode='max',
                              factor=0.5,
                              patience=0,
                              verbose=True)

    if load_from is not None:
        checkpoint = torch.load(load_from)
        start_epoch = checkpoint['epoch'] + 1
        optim.load_state_dict(checkpoint['optim']),
        model.load_state_dict(checkpoint['model'])
        print("Successfully loaded from saved state.")

    print(f"Optimizer: {optim}")
    print(f"Scheduler: {sched}")

    n_train = len(seqs_train)
    n_words = sum(len(words) for words, _, _ in seqs_train)

    #  for it in tqdm(range(start_epoch, start_epoch + n_epochs)):
    for it in range(start_epoch, start_epoch + n_epochs):
        tic = time()
        sample_order = torch.randperm(n_train)

        total_obj = 0
        for k in tqdm(range(n_train)):
            k = sample_order[k]

            word_ix, y_true, _ = seqs_train[k]
            # run model
            model.train()
            optim.zero_grad()
            obj = model.loss(word_ix, y_true)
            total_obj += obj.data[0]
            obj.backward()
            # sanity check that we flow end to end
            # print(model.embed.weight.grad.norm())
            optim.step()

        toc = time()

        print(f'iter {it} took {toc - tic:.2f}s '
              f'train loss {total_obj / n_words:.4f}')

        prec, recall, f1 = score(model, seqs_valid, cuda=cuda)
        sched.step(f1)
        print(f'\t\tvalid micro P: {prec:.2f} R: {recall:.2f} F: {f1:.3f}')

        checkpoint = {
            'model': model.state_dict(),
            'epoch': it,
            'optim': optim.state_dict()
        }

        torch.save(checkpoint, "{}_F_{:.3f}_e{:04d}.pt".format(
            checkpoint_fn,
            f1,
            it))


def main():

    import argparse
    import joblib

    parser = argparse.ArgumentParser()
    parser.add_argument('--data-dir', default='data/ace')
    parser.add_argument('--model', choices=_models.keys(), default='softmax')
    parser.add_argument('--save', default='.')
    parser.add_argument('--load')
    parser.add_argument('--seed', default=42, type=int)
    parser.add_argument('--epochs', default=20, type=int)
    parser.add_argument('--gpuid', default=-1, type=int)
    parser.add_argument('--dropout', default=0.1, type=float)

    opts = parser.parse_args()
    torch.manual_seed(opts.seed)
    if opts.gpuid >= 0:
        torch.cuda.manual_seed(opts.seed)
        torch.cuda.set_device(opts.gpuid)
        cuda = True
    else:
        cuda = False

    print("Loading data...")
    word_dict = joblib.load(os.path.join(opts.data_dir, "vocab.jbl"))
    train_data = joblib.load(os.path.join(opts.data_dir, "train.jbl"))
    valid_data = joblib.load(os.path.join(opts.data_dir, "valid.jbl"))
    embed_ix, embed_weights = joblib.load(os.path.join(opts.data_dir,
                                                       "embed.jbl"))

    embed_ix = torch.from_numpy(embed_ix)
    embed_weights = torch.from_numpy(embed_weights)

    labels = set(y for _, ys in train_data for y in ys)
    label_dict = {y: k for k, y in enumerate(sorted(list(labels)))}

    def _format(seq):
        words, labels = seq
        words = Variable(torch.LongTensor(words))
        label_ixs = [label_dict[y] for y in labels]
        label_ixs = Variable(torch.LongTensor(label_ixs))
        if cuda:
            return words.cuda(), label_ixs.cuda(), labels
        else:
            return words, label_ixs, labels

    train_data = [_format(seq) for seq in train_data]
    valid_data = [_format(seq) for seq in valid_data]

    Model = _models[opts.model]
    model = Model(vocab_size=len(word_dict),
                  label_dict=label_dict,
                  embed_dim=300)

    checkpoint_fn = f'seq_{opts.model}'

    model.initialize_embeddings(embed_ix, embed_weights)
    train(model, train_data, valid_data, n_epochs=opts.epochs,
          checkpoint_fn=checkpoint_fn, load_from=opts.load, cuda=cuda)


if __name__ == '__main__':
    main()
