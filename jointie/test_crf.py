from itertools import product

import pytest
import torch
from torch.autograd import Variable
from scipy.misc import logsumexp

from .seq_models import LSTMCRFTagger

def naive_log_score(y, unaries, initial, final, trans):

    score = initial[y[0]]
    score += unaries[0, y[0]]

    for i in range(1, len(unaries)):
        score += trans[y[i - 1], y[i]]
        score += unaries[i, y[i]]

    score += final[y[-1]]
    return score


def naive_partition_function(unaries, initial, final, trans):

    all_scores = []
    seq_length, n_states = unaries.shape

    for y in product(range(n_states), repeat=seq_length):
        score = naive_log_score(y, unaries, initial, final, trans)
        all_scores.append(score)

    return logsumexp(all_scores)

def naive_argmax(unaries, initial, final, trans):
    all_scores = []
    seq_length, n_states = unaries.shape

    for y in product(range(n_states), repeat=seq_length):
        score = naive_log_score(y, unaries, initial, final, trans)
        all_scores.append((score, list(y)))

    y = sorted(all_scores)[-1]
    return y


def _setup(seq_len, n_states):
    states = "abcdefghijklmn"[:n_states]

    torch.manual_seed(1)
    unary = torch.randn(seq_len, n_states)

    model = LSTMCRFTagger(vocab_size=5,
                          label_dict={a: k for k, a in enumerate(states)},
                          embed_dim=6,
                          hidden_dim=7)

    init = model.initial.data.numpy()
    final = model.final.data.numpy()
    trans = model.transition.data.numpy()

    return model, unary, init, final, trans


def test_seq_score():
    seq_len = 4
    n_states = 3

    model, unary, init, final, trans = _setup(seq_len, n_states)

    for y in ([2, 0, 1, 0], [0, 0, 0, 0], [1, 2, 0, 1]):
        naive_score = naive_log_score(y, unary, init, final, trans)
        y = torch.LongTensor(y)
        pytorch_score = model._seq_score(Variable(unary), y).data.numpy()[0]
        assert (pytorch_score - naive_score) ** 2 < 1e-8


@pytest.mark.parametrize('seq_length', [2, 3, 4])
@pytest.mark.parametrize('n_states', [2, 3, 4])
def test_partition_function(seq_length, n_states):
    model, unary, init, final, trans = _setup(seq_length, n_states)
    pytorch_logz = model._partition_function(Variable(unary)).data.numpy()[0]
    naive_logz = naive_partition_function(unary.numpy(), init, final, trans)
    assert (pytorch_logz - naive_logz) ** 2 < 1e-8


@pytest.mark.parametrize('seq_length', [2, 3, 4])
@pytest.mark.parametrize('n_states', [2, 3, 4])
def test_viterbi(seq_length, n_states):
    model, unary, init, final, trans = _setup(seq_length, n_states)
    pytorch_val, pytorch_y = model._viterbi(Variable(unary))
    naive_val, naive_y = naive_argmax(unary.numpy(), init, final, trans)
    assert (pytorch_val.data[0] - naive_val) ** 2 < 1e-8
    assert pytorch_y == naive_y


