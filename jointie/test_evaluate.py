import pytest
from .evaluate import (_to_entities, _count_predictions,
                       bio_f_score_micro, bio_f_score_macro)


@pytest.mark.parametrize('cls', ['PER', 'ORG'])
@pytest.mark.parametrize('n', (3, 1, 0))
def test_to_entities_empty(cls, n):

    case = ['O'] * n
    assert _to_entities(case, cls) == []


def test_to_entities_count_unit():
    case = ['U_PER', 'U_PER', 'U_PER', 'U_ORG', 'U_ORG']
    assert len(_to_entities(case, 'PER')) == 3
    assert len(_to_entities(case, 'ORG')) == 2
    assert _to_entities(case, 'LOC') == []


def test_to_entities_identify_segment():
    case = ['O', 'B_PER', 'I_PER', 'I_PER', 'L_PER', 'O']
    assert _to_entities(case, 'PER') == [(1, 4)]
    assert _to_entities(case, 'ORG') == []


def test_to_entities_edge_incomplete_boundary():
    case = ['O', 'B_PER']
    assert _to_entities(case, 'PER') == []
    case = ['O', 'B_PER', 'I_PER']
    assert _to_entities(case, 'PER') == []
    case = ['L_PER', 'O']
    assert _to_entities(case, 'PER') == []
    case = ['I_PER', 'L_PER', 'O']
    assert _to_entities(case, 'PER') == []
    assert _to_entities(case, 'PER') == []


def test_to_entities_edge_interrupt_o():
    case = ['O', 'B_PER', 'O']
    assert _to_entities(case, 'PER') == []
    case = ['O', 'B_PER', 'O', 'L_PER']
    assert _to_entities(case, 'PER') == []
    case = ['O', 'B_PER', 'O', 'I_PER', 'L_PER']
    assert _to_entities(case, 'PER') == []


def test_to_entities_edge_interrupt_i():
    case = ['O', 'B_PER', 'I_ORG']
    assert _to_entities(case, 'PER') == []
    case = ['O', 'B_PER', 'I_ORG', 'L_PER']
    assert _to_entities(case, 'PER') == []
    case = ['O', 'B_PER', 'I_ORG', 'I_PER', 'L_PER']
    assert _to_entities(case, 'PER') == []
    assert _to_entities(case, 'ORG') == []


def test_to_entities_edge_interrupt_u():
    case = ['O', 'B_PER', 'U_ORG']
    assert _to_entities(case, 'PER') == []
    assert _to_entities(case, 'ORG') == [(2, 2)]
    case = ['O', 'B_PER', 'U_ORG', 'L_PER']
    assert _to_entities(case, 'PER') == []
    assert _to_entities(case, 'ORG') == [(2, 2)]
    case = ['O', 'B_PER', 'U_PER', 'L_PER']
    assert _to_entities(case, 'PER') == [(2, 2)]


def test_count_predictions():

    true = ['O', 'U_PER', 'U_ORG', 'U_ORG', 'U_PER', 'U_PER']
    pred = ['U_PER', 'U_PER', 'U_PER', 'U_PER', 'O', 'O']
    assert _count_predictions(true, pred, 'PER') == (1, 3, 2)
    assert _count_predictions(true, pred, 'ORG') == (0, 0, 2)


def test_f_scores():
    true = ['O', 'U_PER', 'U_ORG', 'U_ORG', 'U_PER', 'U_PER']
    pred = ['U_PER', 'U_PER', 'U_PER', 'U_PER', 'O', 'O']

    assert bio_f_score_micro(true, pred, []) == (0, 0, 0)
    p, r, f = bio_f_score_micro(true, pred, ['PER'])
    assert p == 1 / 4
    assert r == 1 / 3
    assert round(f, 5) == round(2 / 7, 5)

    # check order invar
    assert (bio_f_score_micro(true, pred, ['PER', 'ORG']) ==
            bio_f_score_micro(true, pred, ['ORG', 'PER']))

    assert (round(bio_f_score_macro(true, pred, ['PER'])[0], 5) ==
            round(2 / 7, 5))

    macro_f = sum(bio_f_score_macro(true, pred, ['PER', 'ORG'])) / 2
    _, _, micro_f = bio_f_score_micro(true, pred, ['PER', 'ORG'])
    assert(macro_f < micro_f)


def test_f_edge_cases():
    assert bio_f_score_macro([], [], ['PER', 'ORG']) == [0, 0]
    assert bio_f_score_micro([], [], ['PER', 'ORG']) == (0, 0, 0)

